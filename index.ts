import { Order, Product } from './types';

class OrderProcessingSystem {
  private products: Product[] = [];
  private orders: Order[] = [];

  addProduct(productId: number, productName: string, price: number) {
    try {
      if (!productId || !productName || price <= 0) {
        throw new Error('Invalid input for adding a product.');
      }

      const product = new Product(productId, productName, price);
      this.products.push(product);
      return `Product "${productName}" added successfully.`;
    } catch (error) {
      console.log(error);
    }
  }

  removeProduct(productId: number) {
    try {
      if (!productId) {
        throw new Error('Invalid input for removing a product.');
      }

      const index = this.products.findIndex(
        (product) => product.id === productId,
      );
      if (index === -1)
        throw new Error(`Product with ID ${productId} not found.`);

      const removedProduct = this.products.splice(index, 1)[0];
      return `Product "${removedProduct.name}" removed successfully.`;
    } catch (error) {
      console.log(error);
    }
  }

  placeOrder(orderId: number, orderedProductIds: number[]) {
    try {
      if (!orderId || orderedProductIds.length === 0) {
        throw new Error('Invalid input for placing an order.');
      }

      const orderedProducts = orderedProductIds.map((productId) => {
        return this.products.find((product) => product.id === productId);
      });

      const isProductEmpty = orderedProducts.every(
        (product) => product !== undefined,
      );

      if (!isProductEmpty)
        throw new Error('One or more products in the order were not found.');

      const order = new Order(orderId, orderedProducts as Product[]);
      this.orders.push(order);
      return `Order with ID ${orderId} placed successfully.`;
    } catch (error) {
      console.log(error);
    }
  }

  cancelOrder(orderId: number) {
    try {
      if (!orderId) {
        throw new Error('Invalid input for canceling an order.');
      }

      const index = this.orders.findIndex((order) => order.id === orderId);
      if (index === -1) throw new Error(`Order with ID ${orderId} not found.`);

      const canceledOrder = this.orders.splice(index, 1)[0];
      return `Order with ID ${orderId} canceled successfully.`;
    } catch (error) {
      console.log(error);
    }
  }

  listProducts() {
    console.log('Products:');
    this.products.forEach((product) => {
      console.log(
        `ID: ${product.id}, Name: ${product.name}, Price: ${product.price}`,
      );
    });
  }

  listOrders() {
    console.log('Orders:');
    this.orders.forEach((order) => {
      const productNames = order.products
        .map((product) => product.name)
        .join(', ');
      console.log(`ID: ${order.id}, Products: ${productNames}`);
    });
  }
}

const orderSystem = new OrderProcessingSystem();

orderSystem.addProduct(1, 'Product A', 10);
orderSystem.addProduct(2, 'Product B', 20);
orderSystem.listProducts();

orderSystem.placeOrder(101, [1, 2]);
orderSystem.placeOrder(102, [1]);
orderSystem.listOrders();

orderSystem.cancelOrder(101);
orderSystem.listOrders();
