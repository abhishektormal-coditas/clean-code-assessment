import { Product } from './product.type';

export class Order {
  constructor(public id: number, public products: Product[]) {}
}
